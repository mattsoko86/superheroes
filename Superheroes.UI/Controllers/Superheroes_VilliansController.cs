﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Superheroes.DATA;

namespace Superheroes.UI.Controllers
{   
    [Authorize(Roles = "Admin, Superheroes")]
    public class Superheroes_VilliansController : Controller
    {
        private SuperheroesEntities db = new SuperheroesEntities();

        // GET: Superheroes_Villians
        public ActionResult Index()
        {
            var superheroes_Villians = db.Superheroes_Villians.Include(s => s.Alignment);
            return View(superheroes_Villians.ToList());
        }
        
        // GET: Superheroes_Villians/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Superheroes_Villians superheroes_Villians = db.Superheroes_Villians.Find(id);
            if (superheroes_Villians == null)
            {
                return HttpNotFound();
            }
            return View(superheroes_Villians);
        }
        [Authorize(Roles = "Admin")]
        // GET: Superheroes_Villians/Create
        public ActionResult Create()
        {
            ViewBag.AlignmentID = new SelectList(db.Alignments, "AlignmentID", "AlignmentType");
            return View();
        }

       
        // POST: Superheroes_Villians/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "HeroID,HeroName,Alias,Origin,AlignmentID")] Superheroes_Villians superheroes_Villians)
        {
            if (ModelState.IsValid)
            {
                db.Superheroes_Villians.Add(superheroes_Villians);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AlignmentID = new SelectList(db.Alignments, "AlignmentID", "AlignmentType", superheroes_Villians.AlignmentID);
            return View(superheroes_Villians);
        }

        // GET: Superheroes_Villians/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Superheroes_Villians superheroes_Villians = db.Superheroes_Villians.Find(id);
            if (superheroes_Villians == null)
            {
                return HttpNotFound();
            }
            ViewBag.AlignmentID = new SelectList(db.Alignments, "AlignmentID", "AlignmentType", superheroes_Villians.AlignmentID);
            return View(superheroes_Villians);
        }


        // POST: Superheroes_Villians/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "HeroID,HeroName,Alias,Origin,AlignmentID")] Superheroes_Villians superheroes_Villians)
        {
            if (ModelState.IsValid)
            {
                db.Entry(superheroes_Villians).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AlignmentID = new SelectList(db.Alignments, "AlignmentID", "AlignmentType", superheroes_Villians.AlignmentID);
            return View(superheroes_Villians);
        }

        // GET: Superheroes_Villians/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Superheroes_Villians superheroes_Villians = db.Superheroes_Villians.Find(id);
            if (superheroes_Villians == null)
            {
                return HttpNotFound();
            }
            return View(superheroes_Villians);
        }

        // POST: Superheroes_Villians/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Superheroes_Villians superheroes_Villians = db.Superheroes_Villians.Find(id);
            db.Superheroes_Villians.Remove(superheroes_Villians);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
       
    }
}
