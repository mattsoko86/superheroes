﻿using System.Web.Mvc;

namespace Superheroes.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult About()
        {
          return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Activities()
        {
            return View(); 
        }
        

    }//end class HomeController
}//end namespace
