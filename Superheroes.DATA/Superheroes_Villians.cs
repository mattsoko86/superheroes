//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Superheroes.DATA
{
    using System;
    using System.Collections.Generic;
    
    public partial class Superheroes_Villians
    {
        public int HeroID { get; set; }
        public string HeroName { get; set; }
        public string Alias { get; set; }
        public string Origin { get; set; }
        public int AlignmentID { get; set; }
    
        public virtual Alignment Alignment { get; set; }
    }
}
